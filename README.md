Dash.by API Library
===================

v0.0.1
------

This is alpha-level software used to interface with the Dash.by API as defined on:
https://dash.by/resources.html

This is NOT production-ready, and should be used at your own peril.

Installation
------------

The preferred method of installation is with Composer.  Once Composer is installed,
install this library by command line:
        
```
#!bash

php composer.phar require stevepacker/dashby
```


Alternatively, you can directly modify your composer.json file to include it in the
"require" section:

```
#!json
        {
            "require": {
                "stevepacker/dashby": "*"
            }
        }
```


Usage
-----

See example.php file.


License
-------
Licensed using the MIT license.

    Copyright (c) 2014 Stephen Packer https://bitbucket.org/stevepacker

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.