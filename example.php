<?php
require 'vendor/autoload.php';

// Since we aren't autoloading through composer, add a lightweight version.
// This wouldn't be needed when including this in your own project.
// From: http://stackoverflow.com/a/14831482/3938550
spl_autoload_register(function ($c) {
    $c = str_replace('stevepacker\dashby', 'src', $c);
    @include preg_replace('#\\\|_(?!.+\\\)#','/',$c).'.php';
});

// Is this a CLI application?  If so, fake $_SESSION
if (php_sapi_name() == "cli") {
    $_SESSION = [];
    $_SERVER['REQUEST_URI'] = '/';
    $_SERVER['HTTP_HOST']   = 'www.example.com';
    if (isset($argv[1])) {
        $_SERVER['REQUEST_URI'] .= '?' . $argv[1];
    }
}

/**
 * The following code is not production-worthy code.
 *
 * It is simple and flawed to more-clearly demonstrate the SDK.
 */

$apiClientId     = getenv('DASHBY_ID')     ?: 'exampleId';
$apiClientSecret = getenv('DASHBY_SECRET') ?: 'exampleSecret';
$api = new \stevepacker\dashby\Api($apiClientId, $apiClientSecret);

// You can register for webhook data pushes from Dash.by.  You can probably think up
// a better way to do this.
// AFAIK you only have to do this once per website, not per User.
$webhookFile = __DIR__ . '/.dashby-webhook-registered';
if (! file_exists($webhookFile)) {
//    $api->webhookRegister();
//    touch($webhookFile);
}

// When this file receives a webhook, do something with it.  In this example, write
// its data to a date-stamped file.
if (isset($_GET['payload'], $_GET['eventType'], $_GET['userId'])) {
    $webhook = $api->webhookParse();
    $logFile = __DIR__ . '/webhookLog-' . date('Y-m-d') . '.log';
    file_put_contents($logFile, print_r($webhook, true), FILE_APPEND);
    die('Webhook payload recorded');
}

// Check to see if we've already obtained a bearer token for this user.
// You should:
//   A) Store the bearer token in encrypted format
//   B) Store the bearer token in something more permanent than a session.
$bearer = isset($_SESSION['dashby-bearer'])
    ? $_SESSION['dashby-bearer']
    : null;

if (! $bearer) {
    // The user may be coming back to here from authorizing our access.
    // This method checks for an auth code in the URL and provides it if it exists.
    $code = $api->getAuthCode();

    // When it does not exist, send the user to Dash.by to authorize us.
    if (! $code) {
        header("Location: " . $api->getAuthUrl());
        die('Redirecting to AuthURL: ' . $api->getAuthUrl());
    }

    // Obtain the bearer token from Dash.by, which is our authorization to get data
    // pertaining to our and Dash.by's mutual User.
    $bearer = $_SESSION['dashby-bearer'] = $api->getAuthBearerTokenFromCode($code);
    if (! $bearer) {
        die('No bearer token obtained.  Did you cancel the authorization process?');
    }
}

// With a bearer token, we can now fetch data:
$dashbyUser = $api->getEndpoint($bearer);
print_r($dashbyUser->getUser());
print_r($dashbyUser->getStats());
print_r($dashbyUser->getBumperstickers());
$trips = $dashbyUser->getTrips('-7 days');
print_r($trips);
$trip  = array_shift($trips); /* @var $trip \stevepacker\dashby\objects\Trip */
if ($trip) {
    print_r($dashbyUser->getRoutes($trip->id));
}
