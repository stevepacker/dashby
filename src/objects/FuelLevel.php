<?php

namespace stevepacker\dashby\objects;

/**
 * FuelLevel
 *
 * A webhook object event that gets fired when the car's fuel level is below a
 * certain threshold.
 *
 * @see https://dash.by/webhooks.html
 * @see https://dash.by/object-types.html?endpoint=/trips#FuelLevel
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class FuelLevel extends BaseObject
{
    public $vehicleId;
    public $fuelLevel;
    public $dateRecorded;
}