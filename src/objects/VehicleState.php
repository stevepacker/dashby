<?php

namespace stevepacker\dashby\objects;

/**
 * VehicleState
 *
 * A webhook event that gets fired when the car's engine is turned on or off.
 *
 * @see https://dash.by/webhooks.html
 * @see https://dash.by/object-types.html?endpoint=/trips#VehicleState
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class VehicleState extends BaseObject
{
    public $vehicleId;
    public $state;
    public $latitude;
    public $longitude;
    public $date;
}