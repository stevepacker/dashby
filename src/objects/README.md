Dash.by API Objects
===================

The objects defined in this folder all extend from BaseObject and are as defined on
https://dash.by/object-types.html?endpoint=/trips#topOfPage