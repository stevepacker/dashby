<?php

namespace stevepacker\dashby\objects;

/**
 * User
 *
 * The User API endpoint describing the user.
 *
 * @see https://dash.by/object-types.html?endpoint=/user#User
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class User extends BaseObject
{
    public $id;
    public $firstName;
    public $lastName;
    public $gender;
    public $userProfileImageUrl;
    /**
     * @var MeasurementUnit
     */
    public $preferredUnits;
    /**
     * @var Vehicle
     */
    public $currentVehicle;
    /**
     * @var Vehicle[]
     */
    public $vehicles = [];
    /**
     * @var int
     */
    public $overallScore;

    public function setAttributes(array $data)
    {
        parent::setAttributes($data);

        if (! empty($data['currentVehicle'])) {
            $this->currentVehicle = new Vehicle($data['currentVehicle']);
        }

        if (! empty($data['vehicles'])) {
            $this->vehicles = [];
            foreach ($data['vehicles'] as $vehicle) {
                $this->vehicles[] = new Vehicle($vehicle);
            }
        }

        if (! empty($data['preferredUnits'])) {
            $this->preferredUnits = new MeasurementUnit($data['preferredUnits']);
        }
    }
}