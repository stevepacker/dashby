<?php

namespace stevepacker\dashby\objects;

/**
 * RoutePoint
 *
 * A snapshot point during a Dash trip.
 *
 * @see https://dash.by/object-types.html?endpoint=/trips#RoutePoint
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class RoutePoint extends BaseObject
{
    public $vehicleId;
    public $latitude;
    public $longitude;
    public $speed;
    public $fuelEfficiency;
    /**
     * @var Alert[]
     */
    public $alerts;
}