<?php

namespace stevepacker\dashby\objects;

/**
 * Stats
 *
 * A generic object to reflect aggregated statistics on one or many trips.
 *
 * @see https://dash.by/object-types.html?endpoint=/trips#Stats
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class Stats extends BaseObject
{
    public $dateStart;
    public $dateEnd;
    public $averageFuelEfficiency;
    public $averageSpeed;
    public $distanceDriven;
    public $timeDriven;
    public $fuelConsumed;
}