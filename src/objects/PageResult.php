<?php

namespace stevepacker\dashby\objects;

/**
 * PageResult
 *
 * A generic container for paginated results from an API endpoint.
 *
 * @see https://dash.by/object-types.html?endpoint=/trips#PageResult
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class PageResult extends BaseObject
{
    public $result;
    public $nextResult;
}