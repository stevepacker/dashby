<?php

namespace stevepacker\dashby\objects;

/**
 * Vehicle
 *
 * A User object contains one or many Vehicle objects.
 *
 * @see https://dash.by/object-types.html?endpoint=/user#Vehicle
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class Vehicle extends BaseObject
{
    public $id;
    public $vin;
    public $make;
    public $model;
    public $year;
    public $name;
    public $odometer;
    public $engineDisplacementLiters;
    public $cityFuelEfficiency;
    public $highwayFuelEfficiency;
    public $tankSize;
    public $fuelType;
    public $vehicleProfileImageUrl;
    public $makeLogoImageUrl;
}