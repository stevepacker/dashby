<?php

namespace stevepacker\dashby\objects;

/**
 * Trip
 *
 * Summarizes information about the user's Dash trip.
 *
 * @see https://dash.by/object-types.html?endpoint=/trips#Trip
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class Trip extends BaseObject
{
    public $id;
    public $dateStart;
    public $dateEnd;
    public $vehicleId;
    public $startAddress;
    public $endAddress;
    public $startLatitude;
    public $endLatitude;
    public $startLongitude;
    public $stopLongitude;
    public $startMapImageUrl;
    public $endMapImageUrl;
    public $score;
    /**
     * @var Stats
     */
    public $stats;
    public $startTemperature;
    public $endTemperature;
    public $startWeatherConditions;
    public $endWeatherConditions;
    public $startWeatherConditionsImageUrl;
    public $endWeatherConditionsImageUrl;
    /**
     * @var Alert[]
     */
    public $alerts = [];

    public function setAttributes(array $data)
    {
        parent::setAttributes($data);

        if (! empty($data['alerts'])) {
            $this->alerts = [];
            foreach ($data['alerts'] as $alert) {
                $this->alerts[] = new Alert($alert);
            }
        }

        if (! empty($data['stats'])) {
            $this->stats = new Stats($data['stats']);
        }
    }
}