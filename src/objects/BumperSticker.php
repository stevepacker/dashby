<?php

namespace stevepacker\dashby\objects;

/**
 * BumperSticker
 *
 * A webhook event that gets fired when the user earns a bumper sticker.
 *
 * @see https://dash.by/webhooks.html
 * @see https://dash.by/object-types.html?endpoint=/trips#BumperSticker
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class BumperSticker extends BaseObject
{
    public $dateEarned;
    public $vehicleId;
    public $title;
    public $description;
    public $bumperStickerImageUrl;
}