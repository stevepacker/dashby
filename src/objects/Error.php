<?php

namespace stevepacker\dashby\objects;

/**
 * Error
 *
 * Resulting object when an API request fails.
 *
 * @see https://dash.by/object-types.html?endpoint=/trips#Error
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class Error extends BaseObject
{
    public $type;
    public $message;
}