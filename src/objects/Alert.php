<?php

namespace stevepacker\dashby\objects;

/**
 * Alert
 *
 * A webhook object that gets fired when the user's car engine light goes on.
 *
 * @see https://dash.by/webhooks.html
 * @see https://dash.by/object-types.html?endpoint=/trips#Alert
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class Alert extends BaseObject
{
    public $vehicleId;
    public $alertType;
    public $dateOccurred;
    public $description;
    public $value;
}