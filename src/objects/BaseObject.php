<?php

namespace stevepacker\dashby\objects;

/**
 * BaseObject
 *
 * All objects from the Dash.by API should extend this to hydrate properly.
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
abstract class BaseObject
{
    const ISO8601_REGEX = '/^(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})(\:(\d{2}))?Z$/';

    public static $dateTimeClass     = '\\DateTime';
    public static $dateTimezoneClass = '\\DateTimezone';
    public static $timezone          = 'UTC';

    function __construct(array $data = [], array $hydrateParams = [])
    {
        $this->setAttributes($data);
    }

    public function setAttributes(array $data)
    {
        foreach ($data as $key => $value) {
            // automatically convert any 8601 timestamp to a DateTime object
            if (is_scalar($value) && preg_match(self::ISO8601_REGEX, $value)) {
                $timezone     = new static::$dateTimezoneClass(static::$timezone);
                $this->{$key} = new static::$dateTimeClass($value, $timezone);
                continue;
            }

            // Attributes that exist in multiple objects can be defined here.
            switch ($key) {
                case 'alerts':
                    $this->{$key}[] = new Alert($value);
                    break;

                default:
                    // do not set on non-existing or private (prefixed by "_") properties
                    if (property_exists($this, $key) && ! strpos($key, '_')) {
                        $this->{$key} = $value;
                    }
            }
        }
    }
}