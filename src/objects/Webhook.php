<?php

namespace stevepacker\dashby\objects;

/**
 * Webhook
 *
 * When a webook event is pushed to this class, and the data thus hydrates.
 *
 * @see https://dash.by/webhooks.html
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class Webhook extends BaseObject
{
    public $userId;
    public $event;
    public $payload;
}