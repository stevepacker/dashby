<?php

namespace stevepacker\dashby\objects;

/**
 * MeasurementUnit
 *
 * A generic object that is used in various objects to reflect measured data points.
 *
 * @see https://dash.by/object-types.html?endpoint=/trips#MeasurementUnit
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class MeasurementUnit extends BaseObject
{
    public $distance;
    public $temperature;
    public $volume;
    public $fuelEfficiency;
}