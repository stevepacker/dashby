<?php

namespace stevepacker\dashby\objects;

/**
 * AccessToken
 *
 * Client access token provided through the Authentication flow.
 *
 * @see https://dash.by/auth-flow.html
 * @see https://dash.by/object-types.html?endpoint=/api/chassis/v1/user#AccessToken
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby\objects
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class AccessToken extends BaseObject
{
    /**
     * The access token.
     *
     * @example DWE567KHJHEWS89SD
     *
     * @var string
     */
    public $access_token;
    /**
     * The type of access token, for eg. bearer
     *
     * @example bearer
     *
     * @var string
     */
    public $token_type;
}