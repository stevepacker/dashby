<?php

namespace stevepacker\dashby;

use stevepacker\dashby\objects\BumperSticker;
use stevepacker\dashby\objects\RoutePoint;
use stevepacker\dashby\objects\Stats;
use stevepacker\dashby\objects\Trip;
use stevepacker\dashby\objects\User;

/**
 * Remote
 *
 * @depends \GuzzleHttp
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class Remote 
{
    /**
     * @var string Base URL used to remotely request data.  Typically set by
     * Api::getEndpoint() method during instantiation.
     */
    public $baseUrl;

    /**
     * @var string Token used by all requests to authorize user data retrieval.
     */
    private $_bearerToken;

    /**
     * Assign a bearerToken to this Remote instance.
     *
     * @param string $bearerToken
     */
    function __construct($bearerToken)
    {
        $this->_bearerToken = $bearerToken;
    }

    /**
     * Returns information about the user (including vehicles array and
     * currentVehicle)
     *
     * @return User|null
     *
     * @scope user
     * @url   https://dash.by/endpoint.html?endpoint=/api/chassis/v1/user
     */
    public function getUser()
    {
        if ($data = $this->fetch('user')) {
            return new User($data);
        }
    }

    /**
     * Provides summary information about the user's Dash trips.
     *
     * Includes a "nextUrl" attribute for pagination
     *
     * @param $startTime string Parsed by strtotime
     * @param $endTime   string Parsed by strtotime
     *
     * @return Trip[]
     *
     * @scope trips
     * @url   https://dash.by/endpoint.html?endpoint=/api/chassis/v1/trips
     */
    public function getTrips($startTime = null, $endTime = null)
    {
        $url = $this->routeUrl('trips');

        if ($startTime) {
            // in milliseconds
            $url->getQuery()->add('startTime', strtotime($startTime) * 1000);
        }
        if ($endTime) {
            // in milliseconds
            $url->getQuery()->add('endTime', strtotime($startTime) * 1000);
        }

        $data = $this->fetch($url);
        if (!empty($data['result'])) {
            $output = [];
            foreach ($data['result'] as $trip) {
                $output[] = new Trip($trip);
            }
            return $output;
        }
    }

    /**
     * Returns route information for a given trip.
     *
     * @return RoutePoint[]
     *
     * @scope trips
     * @url   https://dash.by/endpoint.html?endpoint=/api/chassis/v1/stats
     */
    public function getRoutes($tripId)
    {
        if ($data = $this->fetch('routes/' . urlencode($tripId))) {
            $output = [];
            foreach ($data as $point) {
                $output[] = new RoutePoint($point);
            }
            return $output;
        }
    }

    /**
     * Provides user driving stats collected/computed in the Dash application.
     *
     * @param $startTime string
     * @param $endTime   string
     *
     * @return Stats
     *
     * @scope trips
     * @url   https://dash.by/endpoint.html?endpoint=/api/chassis/v1/stats
     */
    public function getStats($startTime = '-30 days', $endTime = 'now')
    {
        $url = $this->routeUrl('stats');

        // in milliseconds
        $url->getQuery()->add('startTime', strtotime($startTime) * 1000);
        $url->getQuery()->add('endTime',   strtotime($endTime)   * 1000);

        if ($data = $this->fetch($url)) {
            return new Stats($data);
        }
    }

    /**
     * Provides a collection of bumper stickers the user has earned within the Dash
     * application.
     *
     * @return BumperSticker[]
     *
     * @scope user
     * @url   https://dash.by/endpoint.html?endpoint=/api/chassis/v1/bumperstickers
     */
    public function getBumperstickers()
    {
        if ($data = $this->fetch('bumperstickers')) {
            $output = [];
            foreach ($data as $sticker) {
                $output[] = new BumperSticker($sticker);
            }
            return $output;
        }
    }

    /**
     * Convert a scalar route to a \GuzzleHttp\Url object using $this->baseUrl
     *
     * @param string $route
     *
     * @return \GuzzleHttp\Url
     */
    protected function routeUrl($route)
    {
        $url = \GuzzleHttp\Url::fromString(rtrim($this->baseUrl, '/'));
        $url->addPath('/chassis/v1/' . trim($route, '/'));
        return $url;
    }

    /**
     * @param string|\GuzzleHttp\Url $url
     *
     * @return array|null
     */
    protected function fetch($url)
    {
        if (empty($this->_bearerToken)) {
            throw new \InvalidArgumentException('Missing $bearerToken information');
        }

        $url = $url instanceof \GuzzleHttp\Url
            ? $url
            : $this->routeUrl($url);

        $http     = new \GuzzleHttp\Client;
        $request  = $http->createRequest('get', $url);
        $request->addHeader('Authorization', 'Bearer ' . $this->_bearerToken);
        $response = $http->send($request);

        if (200 == $response->getStatusCode()) {
            return $response->json();
        }
    }
}