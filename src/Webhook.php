<?php

namespace stevepacker\dashby;

class Webhook 
{
    protected $api;

    function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     * @param array $types Webhook event types to register for. Only seems to be
     *                     necessary per developer account.
     *
     * @return bool
     */
    public function register($types = null)
    {
        if (empty($this->api->localWebhookUrl)) {
            throw new \RuntimeException('API::localWebhookUrl must be set');
        }

        $eventTypes = [
            'ignition_on',
            'ignition_off',
            'bumpersticker_earned',
            'check_engine_light_alert',
            'low_fuel_alert',
        ];

        if (null === $types) {
            $types = $eventTypes;
        } else {
            $types = $types ?: [];
        }

        $url = \GuzzleHttp\Url::fromString(rtrim($this->api->apiBaseUrl, '/'));
        $url->addPath('/authclient/webhooks/register');

        $payload = [
            'authClientId' => $this->api->clientId,
            'secret'       => $this->api->clientSecret,
            'webHookUrl'   => $this->api->localWebhookUrl,
            'eventTypes'   => $types,
        ];

        $http    = new \GuzzleHttp\Client();
        $request = $http->createRequest('POST', $url, ['json' => $payload]);

        try {
            return $http->send($request);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            return $e->getResponse();
        }
    }

    /**
     * Validate that the data provided is from DashBy
     *
     * @param string $data
     *
     * @return bool
     */
    public function validate($data)
    {
        $expectedValue = base64_encode($this->hmac_sha1(
            $this->api->clientSecret,
            $data
        ));

        return isset($_SERVER['HTTP_X_DASH_SIGNATURE'])
            && $_SERVER['HTTP_X_DASH_SIGNATURE'] == $expectedValue;
    }

    /**
     * Seek or adjust input data so it's an array with all the expected fields.
     *
     * @param string|array|null $data
     *
     * @return array
     */
    protected function webhookNormalizeInput($data = null)
    {
        if (! $data) {
            $data = file_get_contents('php://input');
        }

        if (! $this->validate($data)) {
            return false;
        }

        if (is_scalar($data)) {
            $data = json_decode($data, true);
        }

        if (! is_array($data)) {
            throw new \InvalidArgumentException('$data must be an array or JSON: '
                . var_export($data, true));
        }

        if (! isset($data['payload'], $data['eventType'], $data['userId'])) {
            throw new \InvalidArgumentException('$data missing data');
        }

        return $data;
    }

    /**
     * @param string $key
     * @param string $data
     *
     * @return string
     *
     * @url http://php.net/manual/en/function.hash-hmac.php#105099
     *
     * @codeCoverageIgnore
     */
    protected function hmac_sha1($key, $data)
    {
        // Adjust key to exactly 64 bytes
        if (strlen($key) > 64) {
            $key = str_pad(sha1($key, true), 64, chr(0));
        }
        if (strlen($key) < 64) {
            $key = str_pad($key, 64, chr(0));
        }

        // Outter and Inner pad
        $opad = str_repeat(chr(0x5C), 64);
        $ipad = str_repeat(chr(0x36), 64);

        // Xor key with opad & ipad
        for ($i = 0; $i < strlen($key); $i++) {
            $opad[$i] = $opad[$i] ^ $key[$i];
            $ipad[$i] = $ipad[$i] ^ $key[$i];
        }

        return sha1($opad.sha1($ipad.$data, true));
    }

    /**
     * Parse a webhook request and return the object and userId.
     *
     * @param string|array|null $data When NULL, use php://input
     *
     * @return objects\Webhook
     *
     * @throws \Exception
     */
    public function webhookParse($data = null)
    {
        $data = $this->webhookNormalizeInput($data);

        if (false === $data) {
            return false;
        }

        switch ($data['eventType']) {
            case 'bumpersticker_earned':
                $payload = new objects\BumperSticker();
                break;

            case 'ignition_on':
            case 'ignition_off':
                $payload = new objects\VehicleState();
                break;

            case 'check_engine_light_alert':
                $payload = new objects\Alert();
                break;

            case 'low_fuel_alert':
                $payload = new objects\FuelLevel();
                break;

            default:
                throw new \Exception('Unknown eventType: ' . $data['eventType']);
        }

        $payload->setAttributes($data['payload']);

        $webhook          = new objects\Webhook;
        $webhook->userId  = $data['userId'];
        $webhook->event   = $data['eventType'];
        $webhook->payload = $payload;

        return $webhook;
    }

}