<?php

namespace stevepacker\dashby;

use stevepacker\dashby\objects\Alert;
use stevepacker\dashby\objects\BumperSticker;
use stevepacker\dashby\objects\FuelLevel;
use stevepacker\dashby\objects\VehicleState;
use stevepacker\dashby\Webhook;

/**
 * Class Api
 *
 * @url https://dash.by/resources.html
 *
 * If we do not have a $bearer, generate a URL using getAuthUrl() and redirect
 * visitor to login there.  When completed, they come to $localAuthUrl with a code.
 * We then use that code to get a $bearer token.  Thereafter, Remote is used.
 *
 * We will also start getting webhook notifications for that user (I think).
 *
 * Undocumented API endpoints:
 * POST /api/auth/authenticate {
 *      "@class": "com.dashlabs.dash.model.auth.AccessTokenRequest",
 *      "client_id": clientId,
 *      "response_type": "code",
 *      "scope": scope,
 *      "redirect_uri": redirectUri,
 *      "state": state,
 *      "username": username,
 *      "password": token,
 *      "provider": provider }
 * GET  /api/auth/reset/request/$email
 * POST /api/auth/reset {user: $email, code: $emailedCode, password: $newPassword}
 *
 * @author  Stephen Packer <steve@stevepacker.com>
 * @package stevepacker\dashby
 * @license http://www.opensource.org/licenses/mit-license.html MIT License
 */
class Api
{
    public $apiBaseUrl = 'https://dash.by/api/';
    public $clientId;
    public $clientSecret;
    public $localAuthUrl;
    public $localWebhookUrl;

    private $_webhook;

    /**
     * @param $clientId
     * @param $clientSecret
     */
    function __construct($clientId, $clientSecret, array $params = []) {
        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;

        foreach (['apiBaseUrl', 'localAuthUrl', 'localWebhookUrl'] as $param) {
            if (array_key_exists($param, $params)) {
                $this->{$param} = $params[$param];
            }
        }
    }

    /**
     * Get a URL to use to OAUTH2-grant access to us.
     *
     * @param string $url
     *
     * @return string
     */
    public function getAuthUrl($localAuthUrl = null)
    {
        $url = \GuzzleHttp\Url::fromString($this->apiBaseUrl);
        $url->addPath('auth/authorize');
        $url->setQuery([
                'client_id'     => $this->clientId,
                // https://dash.by/auth-scopes.html
                'scope'         => join(' ', ['user', 'trips']),
                'response_type' => 'code', // does not change, ever.
                'state'         => $this->getAuthState(),
            ]);

        if (null === $localAuthUrl) {
            $localAuthUrl = $this->localAuthUrl;
        }

        if ($localAuthUrl) {
            $url->getQuery()->add('redirect_uri', $localAuthUrl);
        }

        return (string) $url;
    }

    /**
     * Only returns the code provided in the request if the state is validated.
     *
     * @return string|false
     */
    public function getAuthCode()
    {
        if (empty($_GET['code'])) {
            return false;
        }

        return $this->validateAuthState()
            ? $_GET['code']
            : false;
    }

    /**
     * Pulls the state from the request and validates it against the session's state.
     *
     * @param string|null $state
     *
     * @return bool
     */
    public function validateAuthState($state = null)
    {
        if (null === $state) {
            $state = array_key_exists('state', $_GET)
                ? $_GET['state']
                : null;
        }

        if (empty($state)) {
            return false;
        }

        return $state == $this->getAuthState();
    }

    /**
     * After a user authenticates, this is provided by the redirect_uri URL as a
     * means to validate forgery did not occur.
     *
     * @return string
     */
    public function getAuthState()
    {
        $state = array_key_exists(__METHOD__, $_SESSION)
            ? $_SESSION[__METHOD__]
            : null;

        if (null === $state) {
            $state = mt_rand();
            $_SESSION[__METHOD__] = $state;
        }

        return $state;
    }

    /**
     * This should be saved with the Customer data; it should not ever change.
     *
     * @param string $code
     *
     * @return string|null
     */
    public function getAuthBearerTokenFromCode($code)
    {
        if (empty($code)) {
            throw new \InvalidArgumentException('$code is required');
        }

        $url = \GuzzleHttp\Url::fromString($this->apiBaseUrl);
        $url->addPath('auth/token');

        $http    = new \GuzzleHttp\Client();
        $request = $http->createRequest('post', $url);
        $body    = \GuzzleHttp\Stream\Stream::factory(json_encode([
            'client_id'     => $this->clientId,
            'client_secret' => $this->clientSecret,
            'grant_type'    => 'authorization_code', // does not change
            'code'          => $code,
        ]));
        $request->setBody($body);
        $request->setHeader('Content-Type', 'application/json');

        $response = $http->send($request);

        $data = $response->json();

        if ($data && !empty($data['access_token'])) {
            return $data['access_token'];
        }
    }

    /**
     * Instantiates (when not already instantiated) a Remote object for a given
     * Bearer Token.
     *
     * @param string $bearer BearerToken
     *
     * @return Remote
     */
    public function getEndpoint($bearerToken)
    {
        static $bearers = [];

        if (! array_key_exists($bearerToken, $bearers)) {
            $bearers[$bearerToken] = new Remote($bearerToken);
            $bearers[$bearerToken]->baseUrl = $this->apiBaseUrl;
        }

        return $bearers[$bearerToken];
    }

    /**
     * Instantiates a Webhook object to handle Webhook functionality.
     *
     * @return Webhook
     */
    public function getWebhook()
    {
        if (null === $this->_webhook) {
            $this->_webhook = new Webhook($this);
        }

        return $this->_webhook;
    }
}
